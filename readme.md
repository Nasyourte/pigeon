# comment faire un site pigeon

## techno

### back

Pour le back on utilise [Strapi.](https://docs.strapi.io/dev-docs/intro)

Strapi utilise node 18.

Pour changer la version de note :``nvm use 18.0``

1. Créer le type dans content-type builder
2. Créer des entrés dans content-manager
3. Gérer les droits sur l'api PUBLIC, dans ``Settings > Users & Permissions plugin > Role > Public``

Quand je crée mon type:
- Est-ce que j'ai besoin du draft/publish ?
- Est-ce que j'ai des contraintes sur les types des attributs ?

Pour lancer strapi
```shell
npm run develop
```

Recup un commande avec les produits et le client
```http request
http://localhost:1337/api/commandes/13?populate[ligne_commandes][populate]&populate[client]=client
```

### front

Pour lancer le front avec 11ty
```shell
 npx @11ty/eleventy --serve
```

Faire un fetch pour recuperer des data
```js
let megaprod
fetch('http://localhost:1337/api/produits/1')
.then(response => response.json())
.then(JsonData => {
  megaprod = JsonData
  console.log(megaprod)
})
```

Ajouter tous les elements qui ont un id dans un objet
```js
let elems = {}
document.querySelectorAll('[id]').forEach(e => elems[e.id] = e)
```

### panier dans le front

```js
      let cmd = {
          id: 0,
          lignes: {}
      }
      function ajouterAuPanier(produit){
          if(cmd.lignes[produit.id]){
              cmd.lignes[produit.id].qt++
          }else{
              cmd.lignes[produit.id] = {prod: produit, qt: 1}
          }
          elems.panier.show()
      }
```

Envoyer des données au back avec un fetch

```js
fetch('http://localhost:1337/api/clients', {
              method: 'POST',
              headers: {"Content-Type": "application/json"},
              body: JSON.stringify({data: client})
          })
```
